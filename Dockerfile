FROM node:alpine3.16
RUN npm install -g @google/clasp
RUN apk add yq git openssl curl
WORKDIR /clasp
